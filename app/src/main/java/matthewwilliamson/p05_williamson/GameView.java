package matthewwilliamson.p05_williamson;

import android.graphics.ColorFilter;
import android.graphics.ColorMatrix;
import android.graphics.Typeface;
import android.view.MotionEvent;
import android.view.View;
import android.content.Context;
import android.util.AttributeSet;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;

public class GameView extends View {
	private int rave;
    private int score;
    private int end;
    private Random rand = new Random();
	private Paint mBorderPaint;
	private Paint mTextPaint;
	private Timer mTimer;
	private View self;
    private Context myContext;
    private CopyOnWriteArrayList<Fruit> fruits = new CopyOnWriteArrayList<>();

    private final ColorFilter negative_filter = new ColorMatrixColorFilter(new ColorMatrix(
            new float[]{-1.0f, 0, 0, 0, 255,
                    0, -1.0f, 0, 0, 255,
                    0, 0, -1.0f, 0, 255,
                    0, 0, 0, 1.0f, 0}));

    public GameView(Context context, AttributeSet attributeSet) {
		super(context, attributeSet);
        myContext = context;
        mBorderPaint = new Paint();
        mBorderPaint.setTextSize(getResources().getDimensionPixelSize(R.dimen.myFontSize));
        mBorderPaint.setTypeface(Typeface.DEFAULT_BOLD);
        mBorderPaint.setTextAlign(Paint.Align.CENTER);
        mTextPaint = new Paint();
		score = 0;
        rave = 0;
        end = 4;
		self = this;
		mTimer = new Timer();
	}

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        mTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                self.postInvalidate();
                rave++;

                // Rave party stuff
                if (rave % 12 == 0) {
                    int r = rand.nextInt(255);
                    int g = rand.nextInt(255);
                    int b = rand.nextInt(255);
                    mBorderPaint.setColor(Color.rgb(r, g, b));
                    mTextPaint.set(mBorderPaint);
                    mTextPaint.setColorFilter(negative_filter);
                }

                // Adding fruits
                if (rave % 12 == 0 && fruits.size() < 5) {
                    fruits.add(new Fruit(myContext, self));
                    rave = 0;
                }
            }
        }, 0, 100);
    }

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		int action = event.getAction() & MotionEvent.ACTION_MASK;
		if (action == MotionEvent.ACTION_DOWN || action == MotionEvent.ACTION_MOVE) {
			int index = event.getActionIndex();
            for (Fruit f: fruits) {
               if (f.hit(event.getX(index), event.getY(index))) {
                   fruits.remove(f);
                   score += 10;
               }
            }
            this.postInvalidate();
		}
        return true;
	}

	protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
		canvas.drawRect(0, 0, getWidth(), getHeight(), mBorderPaint);

        // Game over screen
        if (end == 0) {
            end--;
            canvas.drawText("Game Over!", getWidth() / 2, 200, mTextPaint);
            canvas.drawText("Score: " + score, getWidth() / 2, getHeight() - 200, mTextPaint);
            score = 0;
            fruits.clear();
            return;
        }
        else if (end == -1) {
            end = 4;
            try {
                Thread.sleep(5000);
            }
            catch (Exception e) {
            }
        }

        // Main game screen
        canvas.drawText("Fruit Ninja Rave!", getWidth()/2, 200, mTextPaint);
        canvas.drawText("Style?", getWidth()/2, getHeight() - 200, mTextPaint);

        for (Fruit f: fruits) {
            f.update();
            if (getHeight() < f.getY()) {
                fruits.remove(f);
                end--;
            }
            else {
                f.draw(canvas);
            }
        }
    }
}

