README

Project 5: Android: Fruit Ninja

Fruit Ninja rave party edition. Game was tuned on my emulator which was a bit laggy. Not sure how the game may play on your device. Possible the fruit don't go nearly as far, or the game might play really really quickly as I tuned it to the emulator. I played it on GenyMotion Nexus 5X.

Name: Matthew Williamson

Email: mwilli20@binghamton.edu

B-Number: B00335701
